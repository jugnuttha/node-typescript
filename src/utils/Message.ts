import * as string from 'string.json'

export default class MessageUtil{
    public static getErrorText(code: string, lang = "th" ) : string {
        return string[lang].error[code]
    }
}