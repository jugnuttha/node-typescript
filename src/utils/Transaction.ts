import { Request } from 'express'
import Logger from 'managers/LogManager';

export default class Transaction {
    private _startTime : number;

    constructor(private _req: Request ,public log : Logger = new Logger()){
        this._startTime = Date.now();
        log.setLabel(_req.method+_req.originalUrl)
        log.info("Transaction","incomming request language: "+_req.headers['language'] +" body:"+JSON.stringify(_req.body))
    }

    public getTransactionId() : string {
        return this.log.getTransactionId()
    }

    public getUrlPath() : string {
        return this._req.originalUrl
    }

    public getResponseTime(){
        return Date.now() - this._startTime
    }
}