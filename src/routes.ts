// https://itnext.io/building-restful-web-apis-with-node-js-express-mongodb-and-typescript-part-2-98c34e3513a2

import {Request, Response, NextFunction} from "express";
import { dataController } from "controllers/ControllerTemplate";
import { NotFound } from "./controllers/HttpError"

export class Routes {       
    private dataController = new dataController();

    private notFound = new NotFound();

    public routes(app): void {          
        app.route('/')
        .get((req: Request, res: Response) => {            
            res.status(200).send({
                message: 'GET request successfulll!!!!'
            })
        });      
        
        app.route('/add')
            .post(this.dataController.addNewData)
            .get(this.dataController.getData);

        app.route('/private')
            .get((req:Request, res:Response, next: NextFunction) => {
                // middleware          
                if(req.query.key !== '78942ef2c1c98bf10fca09c808d718fa3734703e'){
                    res.status(401).send('You shall not pass!');
                } else {
                    next();
                }    
            },this.dataController.getData)

        app.route('*')
            .get(this.notFound.response);
    }

}