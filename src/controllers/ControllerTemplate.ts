// https://itnext.io/building-restful-web-apis-with-node-js-express-mongodb-and-typescript-part-3-d545b243541e

import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import { TemplateSchema } from 'models/mongoose/ModelTemplate';

export class dataController{

    public addNewData (req: Request, res: Response) {                
        let newData = new TemplateSchema(req.body);
    
        newData.save((err, data) => {
            if(err){
                res.send(err);
            }    
            res.json(data);
        });
    }

    public getData (req: Request, res: Response) {           
        TemplateSchema.find({}, (err, data) => {
            if(err){
                res.send(err);
            }
            res.json(data);
        });
    }
    
}