import { Request, Response } from 'express';
import ResponseManager from 'managers/ResponseManager';

export class NotFound{
    public response (req: Request, res: Response) {
        ResponseManager.notFound(res)
    }
}
