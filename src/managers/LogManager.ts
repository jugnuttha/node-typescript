import * as winston from 'winston';
import * as DailyRotateFile from 'winston-daily-rotate-file';
import * as moment from 'moment-timezone';
import * as cluster from 'cluster'
import { Configuration } from 'Configuration';

export default class Logger {
    private _log;
    private _label : string;

    public label: string 
        get() {
            return this.label
        }
        set(value) {
            this.label = value
        }

    constructor(private _trxId:string= null){
        if ( _trxId === null) {
            this._trxId=Math.round((new Date()).getTime()).toString().substr(3)+ Math.round((Math.random() * 899 +100)).toString();
        }       
        const  transports = [new DailyRotateFile({ 
            filename: Configuration.LOG_PATH+'/'+Configuration.LOG_FILENAME+'_%DATE%.log',
            datePattern: 'YYYYMMDD',
           // maxSize: '20m',
            maxFiles: Configuration.LOG_MAX
          })];

          const myFormat = winston.format.printf(info => {
            let workerId = "0:"
              if ( Configuration.CLUSTER ){
                if ( cluster.isWorker ) {
                    workerId = cluster.worker.id+":"
                }
              }
            const message = `${info.timestamp} #${workerId}${info.tid} ${info.level} [${info.label}:${info.tag}] ${info.message}`
            if ( Configuration.LOG_CONSOLE ) console.log(message);
            return message;
          });
        
        const appendTimestamp = winston.format((info, opts) => {
            if(opts.tz)
              info.timestamp = moment().tz(opts.tz).format();
            return info;
        });

        this._log = winston.createLogger({
     //       level: config.log_level,
            format: winston.format.combine(
                appendTimestamp({ tz: 'Asia/Bangkok' }),
                winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss.SSS'}),
                myFormat
              ),
            transports: transports
        })
    }

    public setTransactionId(trxId: string): void {
        this._trxId=trxId;
    }

    public getTransactionId(): string {
        return this._trxId;
    }

    public setLabel(label: string){
        this._label = label;
    }

    public getLabel(){
        return this._label;
    }

    public dev(tag: string, msg:string): void{
        if ( Configuration.LOG_DEV ) 
            this._log.info({tid:this._trxId,label:this._label, tag, message:msg});
    }

    public info(tag: string, msg:string): void{
        this._log.info({tid:this._trxId,label:this._label, tag, message:msg});
    }

    public warn(tag: string, msg:string): void{
        this._log.warn({tid:this._trxId,label:this._label, tag, message:msg});
    }

    public error(tag: string, msg:string): void{
        this._log.error({tid:this._trxId,label:this._label, tag, message:msg});
    }

}

