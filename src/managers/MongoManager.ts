import * as mongoose from 'mongoose';
import Logger from 'managers/LogManager';
import { Configuration } from 'Configuration';

export default class MongoManager {
    private tag = 'MongoManager';
    private log :Logger;

    constructor(){
        this.log = new Logger("0");

        const mongoServerURI = Configuration.MONGO_AUTHEN_TYPE+"://" + Configuration.MONGO_ADDRESS +"?authSource=" + Configuration.MONGO_AUTHEN_DB
        const mongoConnectionOptions : mongoose.ConnectionOptions = {
            useCreateIndex: true,
            user: Configuration.MONGO_USER,
            pass: Configuration.MONGO_PASSWORD,
            dbName: Configuration.MONGO_DB,
            useNewUrlParser: true,
            useFindAndModify: false,
            autoReconnect: true,
            reconnectInterval: 1000,
            reconnectTries: Number.MAX_VALUE, 
            poolSize: 10, // Maintain up to 10 socket connections
            connectTimeoutMS: 5000, // Give up initial connection after 10 seconds
            socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
          };
        
        mongoose.connect(mongoServerURI, mongoConnectionOptions);

        mongoose.connection.on('connected', () => {
            this.log.info(this.tag,"Mongoose default connection is open to "+ mongoServerURI);
        });
    
        mongoose.connection.on('error',  (err) => {
            this.log.info(this.tag, "Mongoose default connection has occured " + err);
        });
    
        mongoose.connection.on('disconnected',  () => {
            this.log.info(this.tag, "Mongoose default connection is disconnected" );
        });
    
        mongoose.connection.on('reconnected',  () =>{
            this.log.info(this.tag, "Mongoose default connection is reconnected" );
        });
    
    }


}