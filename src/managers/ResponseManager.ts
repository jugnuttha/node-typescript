import { Response } from 'express';
import MessageUtil from 'utils/Message';
import Transaction from 'utils/Transaction'

interface ApiResponseDao {
    code:string,
    message:string,
    time:number,
    data?:Object
}

export default class ResponseManager{
    private static tag ="ResponseManager";

    public static success(res: Response, data?: any) :void {
        const trx: Transaction = res.locals.trx;
        const apiResponse :ApiResponseDao = {code:"0",message:"success",time:trx.getResponseTime(),data};
        trx.log.info(this.tag,"success: "+JSON.stringify(apiResponse) )
        res.json(apiResponse);
    }

    public static notFound(res: Response) :void {
        res.locals.trx.log.info(this.tag,"Not Found" )
        res.status(404).send();
    }

    public static business(res: Response, code : string, message = "") :void {
        this.fail(510, res, code , message)
    }

    public static technical(res: Response, code : string, message = "") :void {
        this.fail(520, res, code , message)
    }

    private static fail(status: number, res: Response, code : string, message = ""){
        
        if ( message === "" ) {
            message = MessageUtil.getErrorText(code)
        }
        const apiResponse :ApiResponseDao = {code,message,time:res.locals.trx.getResponseTime()};
        res.locals.trx.log.info(this.tag,"faile B"+code+": "+apiResponse.message )
        res.status(status).json(apiResponse);
    }

}