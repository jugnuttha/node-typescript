import * as config from "config.json";

export class Configuration {
    public static PORT:string;
    public static CLUSTER:boolean;
    public static THREAD:number;

    public static LOG_PATH:string;
    public static LOG_FILENAME:string;
    public static LOG_MAX:string;
    public static LOG_DEV:boolean;
    public static LOG_CONSOLE:boolean;

    public static MONGO_AUTHEN_TYPE: string;
    public static MONGO_ADDRESS:string;
    public static MONGO_USER:string;
    public static MONGO_PASSWORD:string;
    public static MONGO_DB:string;
    public static MONGO_AUTHEN_DB:string;

    public static init(process: NodeJS.Process){
        Configuration.PORT = process.env.PORT ? process.env.PORT : config.server.port;
        Configuration.CLUSTER = process.env.CLUSTER ? process.env.CLUSTER === "true" : config.cluster.active ;
        Configuration.THREAD = process.env.THREAD ? parseInt(process.env.THREAD) : config.cluster.thread;
        
        Configuration.LOG_PATH = process.env.LOG_PATH ? process.env.LOG_PATH : config.log.path;
        Configuration.LOG_FILENAME = process.env.LOG_FILENAME ? process.env.LOG_FILENAME : config.log.filename;
        Configuration.LOG_MAX = process.env.LOG_MAX ? process.env.LOG_MAX : config.log.max;
        Configuration.LOG_DEV = process.env.LOG_DEV ? process.env.LOG_DEV === "true" : config.log.dev_level ;
        Configuration.LOG_CONSOLE = process.env.LOG_CONSOLE ? process.env.LOG_CONSOLE === "true" : config.log.console ;

        Configuration.MONGO_AUTHEN_TYPE = process.env.MONGO_AUTHEN_TYPE ? process.env.MONGO_AUTHEN_TYPE : config.mongo.auth_type
        Configuration.MONGO_ADDRESS = process.env.MONGO_ADDRESS ? process.env.MONGO_ADDRESS : config.mongo.address
        Configuration.MONGO_USER = process.env.MONGO_USER ? process.env.MONGO_USER : config.mongo.user
        Configuration.MONGO_PASSWORD = process.env.MONGO_PASSWORD ? process.env.MONGO_PASSWORD : config.mongo.password
        Configuration.MONGO_DB = process.env.MONGO_DB ? process.env.MONGO_DB : config.mongo.database
        Configuration.MONGO_AUTHEN_DB = process.env.MONGO_AUTHEN_DB ? process.env.MONGO_AUTHEN_DB : config.mongo.auth_database


    }

}