import * as mongoose from 'mongoose';

export interface TemplateModel extends mongoose.Document {
    firstName: string; 
    lastName: string; 
    email: string; 
    company: string;
    phone: string;
    created_date: Date;
};

const schema = new mongoose.Schema({
    firstName: {
        type: String,
        required: 'Enter a first name'
    },
    lastName: {
        type: String,
        required: 'Enter a last name'
    },
    email: {
        type: String            
    },
    company: {
        type: String            
    },
    phone: {
        type: Number            
    },
    created_date: {
        type: Date,
        default: Date.now
    }
})

export const TemplateSchema = mongoose.model<TemplateModel>('templates', schema ,'templates');