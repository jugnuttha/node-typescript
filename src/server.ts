import App from "./app"
import * as os from 'os'
import * as cluster from 'cluster'
import * as mongoose from 'mongoose';
import Logger from "managers/LogManager";
import { Configuration } from "Configuration";

Configuration.init(process)

const log = new Logger("0");
log.setLabel("server")
const PORT = Configuration.PORT;

if ( Configuration.CLUSTER ){
    let numThreads = os.cpus().length;

    if ( Configuration.THREAD > 0 ) {
        numThreads = Configuration.THREAD
    }
    
    if (cluster.isMaster) {
        log.dev("server",`Master ${process.pid} is running`);
    
        for (let i = 0; i < numThreads; i++) {
            cluster.fork();
        }
    
        cluster.on('exit', (worker, code, signal) => {
            log.dev("server",`worker ${worker.process.pid} died`);
        });
    }else{
        new App().app.listen(PORT, () => {
            log.dev("server",`Express server pid ${process.pid} listening on port ` + PORT);
        })
    }
}else{
    new App().app.listen(PORT, () => {
        log.dev("server",`Express server listening on port ` + PORT);
    })
}


process.on('SIGINT',  () => {
    mongoose.connection.close(  () => {
        this.log.info(this.tag, "Mongoose default connection is disconnected due to application termination" );
        process.exit(0)
    });
});