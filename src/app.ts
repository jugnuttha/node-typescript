import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors"
import * as useragent from 'express-useragent'

import { Routes } from "./routes";
import Transaction from "utils/Transaction";
import MongoManager from "managers/MongoManager";

export default class App {

    public app: express.Application;
    public route: Routes = new Routes();

    constructor() {
        this.app = express();
        this.config();        
        this.route.routes(this.app);
        new MongoManager();
    }

    private config(): void{
        const transactionInject = function (req, res, next) {
            res.locals.trx = new Transaction(req)
            next()
        }

        this.app.use(useragent.express());

        this.app.use(cors())
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));

        this.app.use(transactionInject);
    }

}