FROM node:10.15.0-alpine

WORKDIR /app

COPY package.json package-lock.json tsconfig.json tsconfig-paths-bootstrap.js ./
RUN npm install

COPY src ./src
RUN npm run build 

EXPOSE 3000

CMD ["npm", "start"]